# site [![Build Status](https://drone.envs.net/api/badges/lfdev/site/status.svg)](https://drone.envs.net/lfdev/site)

## Description
My personal website

### Author

**ldfev**

* [gitea/lfdev](https://git.envs.net/lfdev)
* [gitlab/LFd3v](https://gitlab.com/LFd3v)

### License

Copyright © 2023, [lfdev](https://git.envs.net/lfdev).
Released under the [AGPL 3.0 or later](LICENSE).